package main

import (
	"fmt"
	"log"
	"math/rand"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"time"
)

var filename string
var server_ip uint32
var v bool = false
var vv bool = false
var blk_size int = 512
var server_port uint16 = 69
var client_port uint16
var error_code [8]string = [8]string{
	"Not defined, see error message (if any).",
	"File not found.",
	"Access violation.",
	"Disk full or allocation exceeded.",
	"Illegal TFTP operation.",
	"Unknown transfer ID.",
	"File already exists.",
	"No such user.",
}

// RFC 1350

/* Packet Structure

opcode  operation
            1     Read request (RRQ)
            2     Write request (WRQ)
            3     Data (DATA)
            4     Acknowledgment (ACK)
            5     Error (ERROR)

			Order of Headers

                                                  2 bytes
    ----------------------------------------------------------
   |  Local Medium  |  Internet  |  Datagram  |  TFTP Opcode  |
    ----------------------------------------------------------

TFTP Formats

   Type   Op #     Format without header

          2 bytes    string   1 byte     string   1 byte
          -----------------------------------------------
   RRQ/  | 01/02 |  Filename  |   0  |    Mode    |   0  |
   WRQ    -----------------------------------------------
          2 bytes    2 bytes       n bytes
          ---------------------------------
   DATA  | 03    |   Block #  |    Data    |
          ---------------------------------
          2 bytes    2 bytes
          -------------------
   ACK   | 04    |   Block #  |
          --------------------
          2 bytes  2 bytes        string    1 byte
          ----------------------------------------
   ERROR | 05    |  ErrorCode |   ErrMsg   |   0  |
          ----------------------------------------

		Error Codes

   Value     Meaning

   0         Not defined, see error message (if any).
   1         File not found.
   2         Access violation.
   3         Disk full or allocation exceeded.
   4         Illegal TFTP operation.
   5         Unknown transfer ID.
   6         File already exists.
   7         No such user.

*/

func main() {

	/*
		Get filename
		check file exists
		open file handler
		send request to server
		wait for ack (or data packet for read request) ACK block number will be 0
		figure TID part...
		read 512 bytes from file
		create packet
		send packet
		wait for ack
		repeat until EOF

	*/

	if len(os.Args) < 2 {
		er(fmt.Errorf("no arguments given"))
	}

	rand.Seed(time.Now().UnixNano())
	min := 1024
	max := 65535
	client_port = uint16(rand.Intn(max-min+1) + min)

	args := os.Args[1:]
	for n := 0; n < len(args); n++ {
		arg := args[n]

		if arg == "-h" || arg == "--help" {
			fmt.Print("--file=<FILENAME>\tFilename to send or recieve\n")
			fmt.Print("--ip=<IP ADDR>\t\tIP of remote server\n")
			fmt.Printf("--server_port=<PORT>\tNumber of UDP port on remote server (Default: %d)\n", server_port)
			fmt.Printf("--client_port=<PORT>\tNumber of UDP port on the client (Default: RANDOM [%d])\n", client_port)
			fmt.Print("-v\t\t\tVerbose output\n")
			fmt.Print("-vv\t\t\tVery verbose output\n")
			fmt.Print("-h | --help\t\tPrint this help message\n")

			return
		}

		if arg == "-v" {
			v = true
		}

		if arg == "-vv" {
			vv = true
		}

		regex := regexp.MustCompile(`--file=(.+)`)
		if regex.MatchString(arg) {
			filename = regex.FindStringSubmatch(arg)[1]
		}

		regex = regexp.MustCompile(`--ip=(\d+)\.(\d+)\.(\d+)\.(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)
			server_ip = server_ip + uint32(tmp)
			server_ip = server_ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[2])
			er(err)
			server_ip = server_ip + uint32(tmp)
			server_ip = server_ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[3])
			er(err)
			server_ip = server_ip + uint32(tmp)
			server_ip = server_ip << 8

			tmp, err = strconv.Atoi(regex.FindStringSubmatch(arg)[4])
			er(err)
			server_ip = server_ip + uint32(tmp)

		}

		regex = regexp.MustCompile(`--server_port=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)

			server_port = uint16(tmp)
		}

		regex = regexp.MustCompile(`--client_port=(\d+)`)
		if regex.MatchString(arg) {
			tmp, err := strconv.Atoi(regex.FindStringSubmatch(arg)[1])
			er(err)

			client_port = uint16(tmp)
		}
	}

	if filename == "" {
		er(fmt.Errorf("must specify filename with --file=<FILENAME>"))
	}

	if server_ip == 0 {
		er(fmt.Errorf("must specify IP with --ip=<IP ADDR>"))
	}

	if vv {
		fmt.Printf("Filename:    %s\n", filename)
		fmt.Printf("Block Size:  %d\n", blk_size)
		fmt.Printf("Server IP:   %d.%d.%d.%d\n", byte(server_ip>>24), byte(server_ip>>16), byte(server_ip>>8), byte(server_ip))
		fmt.Printf("Server Port: %d\n", server_port)
		fmt.Printf("Client Port: %d\n\n", client_port)

	}

	mode, err := os.Lstat(filename)
	er(err)

	if vv {
		fmt.Printf("File exists (%s)\n", filename)
	}

	if !mode.Mode().IsRegular() {
		er(fmt.Errorf("file: %s, is not regular file", filename))
	}

	if vv {
		fmt.Printf("Is regular file (%s)\n", filename)
	}

	if vv {
		fmt.Print("Creating UDP socket\n")
	}

	client, err := net.ListenPacket("udp", fmt.Sprintf(":%d", client_port))
	er(err)

	if vv {
		fmt.Print("Creating socket buffer\n")
	}

	for {
		buf := make([]byte, 600)
		_, _, err := client.ReadFrom(buf)
		er(err)

		if vv {
			fmt.Print("Socket established\n")
		}

		sendFile(client)
	}
}

func sendFile(client net.PacketConn) {
	fh, err := os.Open(filename)
	er(err)

	defer fh.Close()
	i := 0
	file_buf := make([]byte, blk_size)
	for {
		n, err := fh.Read(file_buf)
		er(err)
		fmt.Printf("i=%d,n=%d\n%b\n\n", i, n, file_buf)
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
